from workflows.junglee_web_driver import JungleeWebDriver
from workflows.home_page import HomePage
from workflows.registration_page import RegistrationPage
from workflows.logged_in_lobby import LoggedInLobby
from workflows.login_page import LoginPage


class TestHowzatHomePage:

    def setup(self):
        """
        Setup driver object and verify login state before test case.
        """
        self.jungleWebdriver = JungleeWebDriver()
        self.driver = self.jungleWebdriver.get_driver()
        self.driver.get("https://www.howzat.com")
        if self.jungleWebdriver.is_user_logged_in():
            lobby = LoggedInLobby(self.driver)
            lobby.logout()

    def teardown(self):
        """
        Quit web driver object after each test.
        """
        self.driver.quit()

    def test_save_apk(self):
        """
        Test case to download apk from home page.
        Steps:
            1. Open Home page of the website.
            2. Download apk by clicking on download button.
            3. Verify that the file is downloaded.
        """
        print("Step 1: Open Home page of the website.")
        home_page = HomePage(self.driver)
        print("Step 2: Download apk by clicking on download button.")
        home_page.download_apk()
        print("Step 3: Verify that the file is downloaded.")
        # TODO: Assertion for downloading file

    def test_register_user(self):
        """
        Test case to register a new user with random creds.
        Steps:
            1. Open registration page by clicking on register button.
            2. Register as a new user.
            3. Verify that user is registered and logged in.
            4. Logout from right scroll menu.
        """
        print("Step 1: Open registration page by clicking on register button.")
        home_page = HomePage(self.driver)
        home_page.register_button.click()
        registration_page = RegistrationPage(self.driver)
        print("Step 2: Register as a new user.")
        registration_page.register_user()
        print("Step 3: Verify that user is registered and logged in.")
        assert self.jungleWebdriver.is_user_logged_in(), "Registration failed, User is not logged in."
        print("Step 4: Logout from right scroll menu.")
        lobby = LoggedInLobby(self.driver)
        lobby.logout()

    def test_login_user(self):
        """
        Test case to Login a new user with creds used for register.
        Steps:
            1. Open Login page by clicking on login button.
            2. Login with the same creds used for registration.
            3. Verify that user is logged in.
            4. Logout from right scroll menu.
        """
        print("Step 1: Open Login page by clicking on login button.")
        home_page = HomePage(self.driver)
        home_page.login_button.click()
        print("Step 2:  Login with the same creds used for registration.")
        login_page = LoginPage(self.driver)
        login_page.login_user()
        print("Step 3: Verify that user is registered and logged in.")
        assert self.jungleWebdriver.is_user_logged_in(), "Login failed, User is not logged in."
        print("Step 4: Logout from right scroll menu.")
        lobby = LoggedInLobby(self.driver)
        lobby.logout()

