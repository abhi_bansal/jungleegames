# JungleeGames

Here is the link of assignment. 
https://docs.google.com/document/d/1KSSyB9lDE94oD05Schq9gGnIHS-v9qs74VSmhjYQ4kQ/edit

The assignment was to automate workflow of given website in android chrome browser. To do 
that i used the following tools and frameworks:

````
1. Python
2. Pytest
3. Appium
````
You need to setup appium on your machine to run these test cases. Follow th given link to setup.
https://www.swtestacademy.com/how-to-install-appium-on-mac/

Following command to run test cases: 
`pytest -s -v testcases/appiumTest.py`

You can change configurations as per your device in given file: 
`jungleegames/workflows/config.json`


````
Pending TODOs:
1. Assert condition on downloaded file. 
2. Dynamically get the screen dimensions for scrolling.
