import json
import os
import random


class RegistrationPage:
    """
    All registration page related operations will come under this class.
    """
    xpaths = {
        "username_textbox": '/html/body/ion-app/ng-component/ion-nav/page-login/ion-content/div[2]'
                            '/ion-row/div/form/ion-row[1]/ion-col/ion-input/input',
        "password_textbox": '/html/body/ion-app/ng-component/ion-nav/page-login/ion-content/div[2]'
                            '/ion-row/div/form/ion-row[2]/ion-col/ion-input/input',
        "register_button": "/html/body/ion-app/ng-component/ion-nav/page-login/ion-content/div[2]/"
                           "ion-row/div/form/ion-row[4]/button",
        "right_menu_button": "/html/body/ion-app/ng-component/ion-nav/page-lobby/ion-content/div[2]/"
                             "ion-list/button[5]",
        "logout_button": '//*[@id="loggedInMenu"]/div/ion-content/div[2]/ion-row[10]/button',
    }

    def __init__(self, driver):
        """
        Initialize webdriver object and read and write test config file.
        Args:
             driver: Webdriver object to perform different operations.
        """
        self.driver = driver
        with open(os.getcwd() + "/workflows/config.json") as f:
            config = json.load(f)
        f.close()
        # Writing string to json file so can be used during login operations.
        with open(os.getcwd() + "/workflows/config.json", "w") as f:
            self.random_string = str(random.randint(1000, 9999))
            config["random_string"] = self.random_string
            json.dump(config, f)
        f.close()
        self.creds = config["creds"]


    @property
    def username_textbox(self):
        """
        Retrieve username textbox web element.
        Returns:
            "MobileWebElement"
        """
        elem = self.driver.find_element_by_xpath(self.xpaths['username_textbox'])
        return elem

    @property
    def password_textbox(self):
        """
        Retrieve Password textbox web element.
        Returns:
            "MobileWebElement"
        """
        elem = self.driver.find_element_by_xpath(self.xpaths['password_textbox'])
        return elem

    @property
    def register_button(self):
        """
        Retrieve register button web element of form.
        Returns:
            "MobileWebElement"
        """
        elem = self.driver.find_element_by_xpath(self.xpaths['register_button'])
        return elem

    def register_user(self):
        """
        Register user by filling registration form with unique credentials.
        """
        username = self.creds['username'] + "_" + self.random_string + "@jungleegames.com"
        password = self.creds['password'] + "_" + self.random_string
        self.username_textbox.send_keys(username)
        self.password_textbox.send_keys(password)
        self.register_button.click()

