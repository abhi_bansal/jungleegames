import json
import os
import random
from workflows.registration_page import RegistrationPage

class LoginPage:
    """
    All login page related operations will come under this class.
    """
    xpaths = {
        "username_textbox": '/html/body/ion-app/ng-component/ion-nav/page-login/ion-content/div[2]'
                            '/ion-row/div/form/ion-row[1]/ion-col/ion-input/input',
        "password_textbox": '/html/body/ion-app/ng-component/ion-nav/page-login/ion-content/div[2]'
                            '/ion-row/div/form/ion-row[2]/ion-col/ion-input/input',
        "login_button": "/html/body/ion-app/ng-component/ion-nav/page-login/ion-content/div[2]"
                        "/ion-row/div/form/ion-row[4]/button"
    }

    def __init__(self, driver):
        """
        Initialize webdriver object and read test config file.
        Args:
             driver: Webdriver object to perform different operations.
        """
        self.driver = driver
        with open(os.getcwd() + "/workflows/config.json") as f:
            config = json.load(f)
        self.creds = config["creds"]
        self.random_string = config["random_string"]

    @property
    def username_textbox(self):
        """
        Retrieve username textbox web element.
        Returns:
            "MobileWebElement"
        """
        elem = self.driver.find_element_by_xpath(self.xpaths['username_textbox'])
        return elem

    @property
    def password_textbox(self):
        """
        Retrieve password textbox web element.
        Returns:
            "MobileWebElement"
        """
        elem = self.driver.find_element_by_xpath(self.xpaths['password_textbox'])
        return elem

    @property
    def login_button(self):
        """
        Retrieve login button web element.
        Returns:
            "MobileWebElement"
        """
        elem = self.driver.find_element_by_xpath(self.xpaths['login_button'])
        return elem

    def login_user(self):
        """
        Fill the login form using details used during registration and login.
        """
        username = self.creds['username'] + "_" + self.random_string + "@jungleegames.com"
        password = self.creds['password'] + "_" + self.random_string
        self.username_textbox.send_keys(username)
        self.password_textbox.send_keys(password)
        self.login_button.click()
