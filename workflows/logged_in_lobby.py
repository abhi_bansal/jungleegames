
class LoggedInLobby:
    """
    All user home page related operations will come under this class.
    """
    xpaths = {
        "right_menu_button": "/html/body/ion-app/ng-component/ion-nav/page-lobby/ion-content/div[2]/"
                             "ion-list/button[5]",
        "logout_button": '//*[@id="loggedInMenu"]/div/ion-content/div[2]/ion-row[10]/button',
    }

    def __init__(self, driver):
        """
        Initialize webdriver object and read test config file.
        Args:
             driver: Webdriver object to perform different operations.
        """
        self.driver = driver

    @property
    def right_menu_button(self):
        """
        Retrieve right menu button web element.
        Returns:
            "MobileWebElement"
        """
        elem = self.driver.find_element_by_xpath(self.xpaths['right_menu_button'])
        return elem

    @property
    def logout_button(self):
        """
        Retrieve logout button web element from right menu.
        Returns:
            "MobileWebElement"
        """
        elem = self.driver.find_element_by_xpath(self.xpaths['logout_button'])
        return elem

    def logout(self):
        """
        logout by scrolling the side menu and click on logout button.
        """
        import time
        time.sleep(5)
        self.right_menu_button.click()
        elems = self.driver.find_elements_by_xpath(self.xpaths['logout_button'])
        while len(elems) == 0:
            print(len(elems) , 33)
            self.driver.swipe(800, 1900, 800, 300, 10000)
            elems = self.driver.find_elements_by_xpath(
                    self.xpaths['logout_button'])
        elems[0].click()
