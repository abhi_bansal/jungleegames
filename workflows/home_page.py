from workflows.junglee_web_driver import *


class HomePage:
    """
    All home page related operations will come under  this class.
    """
    xpaths = {
        "login_button": '//*[@id="top-bg"]/div[1]/div[2]/div[2]',
        "register_button": '/html/body/div[1]/div/div/div/div[2]/div/div',
        "download_button": '/html/body/div[1]/div/div/div/div[2]/div/a'
    }

    def __init__(self, driver):
        """
        Initialize webdriver object.
        Args:
             driver: Webdriver object to perform different operations.
        """
        self.driver = driver

    @property
    def login_button(self):
        """
        Retrieve login button web element.
        Returns:
            "MobileWebElement"
        """
        elem = self.driver.find_element_by_xpath(self.xpaths['login_button'])
        return elem

    @property
    def register_button(self):
        """
        Retrieve register button web element.
        Returns:
            "MobileWebElement"
        """
        elem = self.driver.find_element_by_xpath(self.xpaths['register_button'])
        return elem

    @property
    def download_button(self):
        """
        Retrieve download apk button web element.
        Returns:
            "MobileWebElement"
        """
        elem = self.driver.find_element_by_xpath(self.xpaths['download_button'])
        return elem

    def download_apk(self):
        """
        download apk by clicking on download button and allow all permission popup.
        """
        self.download_button.click()
        approve_alerts(self.driver)
