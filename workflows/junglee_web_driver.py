import os
import json
import time
from appium import webdriver


class JungleeWebDriver:
    """
    web Driver class initialize the driver and contains all common methods.
    """

    def __init__(self):
        """
        Initialize remote web drive object using the capabilities provided in config file.
        """
        with open(os.getcwd() + "/workflows/config.json") as f:
            config = json.load(f)
        desired_caps = config["desired_cap"]
        desired_caps['chromedriverExecutable'] = os.getcwd() + '/chromedriver'
        self.driver = webdriver.Remote(config['connection_url'], desired_caps)

    def get_driver(self):
        """
        Returns the object of remote web driver.
        Returns:
            "Webdriver"
        """
        return self.driver

    def is_user_logged_in(self):
        """
        Function to check if the user is currently loggedIn or not.
        Returns:
             "Boolean"
        """
        elems = self.driver.find_elements_by_xpath('//*[@id="top-bg"]/div[1]/div[2]/div[2]')
        if len(elems) > 0:
            # Login button is available on the page.
            return False
        return True


def approve_alerts(driver):
    """
    Function to remove alerts coming on download app operations.

    """
    driver.switch_to.context('NATIVE_APP')
    elements = driver.find_elements_by_class_name("android.widget.Button")
    if len(elements) > 0:
        elements[-1].click()
    try:
        driver.switch_to.alert.accept()
        time.sleep(2)
    except:
        print("File access permission alert did not come.")
    elements = driver.find_elements_by_id("com.android.chrome:id/button_primary")
    if len(elements) > 0:
        elements[0].click()
